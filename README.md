# Docker Compose sceleton [python + mysql + firebird]
Шаблон для python проэкта. <br>

* Flask
* python-dotenv
* pydevd
* fdb
* mysql-connector-python
* cherrypy
* beautifulsoup4
* Flask-WTF
* Flask-Bootstrap
* authlib
* requests
* pytelegrambotapi
* telebot

## Getting Started

Для начала работы просто склонируй проэкт и следуй инструкции.
  

### Зависимости

Docker [Docker Engine Ubuntu](https://docs.docker.com/install/linux/docker-ce/ubuntu/)<br>
Docker-Compose [Docker Compose](https://docs.docker.com/compose/install/)

### Установка

```
git clone https://gitlab.com/gomylist20/docker_python_skeleton.git
```

или 

```
git clone git@gitlab.com:gomylist20/docker_python_skeleton.git
```

Теперь нужно добавить flask проэкт
```
cd ./src
git clone <path_to_your_project> .
``` 

Запускаем docker и flask приложение
```
docker-compose up -d
```

Посмотреть имя контейнера python
```
docker ps
```

Зайти в контейнер и запустить flask app
```
docker exec -ti <имя_контейнера> /bin/bash
cd app
flask run
```

### FireBird
Порт: 3050
База мапится в ./fbdb

<h4> Достать пароль FireBird</h4>
Посмотреть имя контейнера firebird

```
docker ps
```
Зайти в контейнер и выполнить следующую команду
```
docker exec -ti <имя_контейнера> /bin/bash
tail /firebird/etc/SYSDBA.password
```

## MySql
База мапится в ./mysqldb

Пароль от root : 123456

Порт: 3306  

## Полезная информация
Приложение 
```
http://127.0.0.1:5000
```
PhpMyAdmin
```
http://127.0.0.1:8088
```

Исходники python хранить в ./src
## Автор

* **Alexei Fedorov** - [GitLab](https://gitlab.com/gomylist20)

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details
